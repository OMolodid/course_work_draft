/*

    Написать свою библиотеку компонентов и основываясь на ней, собрать несложный интерфейс на 1-2 страницы.

    Пример готового интерфейса:
    https://coderthemes.com/hyper/layouts/detached/dashboard-crm.html
    https://keenthemes.com/keen/preview/demo2/


    В ней должно быть как минимум 10 компонетов например:

    
    - Breadcrumbs:
        items: Array() -> required
            Item Scheme: { name, link }

    - Button: 
        status: active (default), disabled 
        type: default (default), primary, secondary, link -> required
        size: medium (default), small, large
        icon: придумать реализацию, используя или url картинки или name картинки (можно юзать либу типа fontawesome)

        Demo:
        <Button type="primary" size="medium"> Woohoo! </Button>

    - Loader:
        message : String
        type: 

        Demo:
        <Loader message="Data loading"/>

    - Confirm
        Компонент для подтверждения того или иного действия:

        <Confirm
            title="Do smsng important"
            message="Are you sure?"
            closeMessage="Back to smsng else"
            textarea={true}
            textareaHandler={ handleConfirmComment }
            disabled={ smsng ? true : false}
        >
            <Button type="primary" size="medium" onClick={Click}> Send Message! </Button>
        </Confirm>


    - Tabs:
        Состоит из нескольких компонентов:
        
        1. Tabs:
            title: 
            changeHandler: обработчик активного элемента -> required
            active: id выбранного элемента -> required

        2. Tab:
            label: имя вкладки -> required
            value: id по которому будет совершаться поиск 
            disabled: отключить вкладку
        
        3. TabContainer
            active: id выбранного элемента -> required

        4. TabItem
            children -> required


        Demo:
        <Tabs active={active} onChange={changeHandler}>
            <Tab label="data1"></Tab>
            <Tab label="data2"></Tab>
        </Tabs>
        <TabContainer active={active}>
            <TabItem tab="data1"> Data 1 </TabItem>
            <TabItem tab="data2"> Data 2</TabItem>
        </TabContainer>

    - Slides


    UI Components: 
    - Card 
        size: small, medium, large
    
    - Table
        Sub-Components:
            Talbe
            Row
            Cell

    - Sidebar
        Sub-Components:
        SidebarItem
            title: 
            link:url
            heading: false

    - Pagination:
        pages: 50
        showBy: ''

    - Footer

    - FormItems
        Input
        Select
        FileLoader
        Radio


*/