import React, { Component } from 'react';
import {
    BrowserRouter,
    Route,
    Link,
} from 'react-router-dom';
import './breadcrumbs.css'



class BreadCrumbs extends Component {
    state = {
        activeIndex: 9
    }

    handleChange = (index) => () => {
        this.setState({ activeIndex: index })
    }


    render = () => {
        const { array } = this.props;
        const { activeIndex } = this.state;

        return (
            // <BrowserRouter>
            <div className='bread-crumbs-wrap'>
                <div className='bread-crumbs-container'
                >
                    <ol className='bread-crumbs-list'>
                        {
                            array.map((item, index) => {
                                if (index < (activeIndex + 1)){
                                return (
                                    <li
                                        key={index}
                                        onClick={this.handleChange(index)}
                                    >
                                        <Link
                                            to={item.link}
                                        >
                                            {item.name}
                                        </Link>
                                    </li>
                                )
                            } else {
                                return null
                            }
                            })
                        }

                    </ol>

                </div>
            </div>
            // </BrowserRouter>
        )
    }


}


export default BreadCrumbs