import React from 'react';
import './sidebar.css';

const Sidebar = ({ show, children, action, title }) => {


    if (!show) {
        return (<button
        className='sidebar-button'
            onClick={action}>
            <i class="fas fa-align-justify"></i>
        </button>)
    } else {
        return (
            <>

                <div className="slidebar-backdrop" >
                    <div className="slidebar" >
                        <button
                        className='sidebar-button'
                            onClick={action}><i class="fas fa-align-justify"></i>
                        </button>
                        <h2 className='sidebar-title'>{title}</h2>
                        {children}

                    </div>
                </div>
            </>
        )
    }



}

export default Sidebar;