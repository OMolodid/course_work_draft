import React from 'react';
import {
    Link,
} from 'react-router-dom';
import Icon from './icon';




const SidebarItem = ({array}) => {

        return (
            
            <div >
                <div>
                    <ul className='slidebar-ul'>
                        {
                            array.map((item, index) => {
                                
                                return (
                                    <li
                                        key={index}
                                        className='slidebar-li'
                                    >
                                    {item.icon && <Icon type={item.icon}/>}
                                        <Link
                                            to={item.link}
                                        >
                                            {item.name}
                                        </Link>
                                    </li>
                                )
                          
                            })
                        }

                    </ul>

                </div>
            </div>
         
        )
    }





export default SidebarItem