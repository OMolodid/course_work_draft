import React from 'react';
import './table.css';


const Table = ({ children }) => (
    <table className='wrapper'>
        <tbody className='table'>
            {children}
        </tbody>
    </table>
)

export default Table;