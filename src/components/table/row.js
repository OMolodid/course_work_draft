import React from 'react';

const Row = ({ head, children }) => {
    return (
        <tr
            className={head ? "row header" : "row"}
        >
            {children}
        </tr>

    )

}

Row.defaultProps = {
    head: false
};


export default Row;