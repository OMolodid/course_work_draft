import React from 'react';


const Cell = ({ children,  type, currency,  }) => {
    if (type === "money") {
        if (currency) {
            return (
                <td
                
                className={type + ' cell'}
            >
                {`${children} ${currency}`}
            </td>
            )
            
        } else {
            return (
                <span>You need currency for type MONEY.</span>
                
            )

        }
    } else {

    return (
            <td
                className={type + ' cell'}
            >
                {children}
            </td>
        )
}
}

Cell.defaultProps = {
    
    type: "text",

};



export default Cell;