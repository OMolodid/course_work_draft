import React from 'react';
import './card.css';


const Card = ({ size, children, src, title, subtitle, content }) => {
    return (
        <div className={'card ' + size}>
            <div class="meta">
                {/* <div class="photo" style={`background-image: url(${src})`}></div> */}
                <div class="photo">
                    <img src={src}></img>
       </div>
                </div>
                <div class="description">
                    <h1>{title}</h1>
                    <h2>{subtitle}</h2>
                    <p> {content}</p>
                    <p class="read-more">
                        {children}
                    </p>
                </div>
            </div>
            )
            
            }
            
            export default Card;
