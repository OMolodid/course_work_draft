import React from 'react';

const Tab = ({disabled, action, isActive, children, index}) => 
<div
  onClick={disabled ? null : action(index) }
  className={disabled ? 'disabledTab' : (
    isActive ? 'activeTab' : 'tab'
  )}
>
  {children}
</div>

export default Tab;


