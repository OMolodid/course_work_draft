import React from 'react';

const TabPanels = ({children, activeIndex}) => 

<div className='tabPanels'>
  {children[activeIndex]}
</div>


export default TabPanels;