import React from 'react';

const TabList = ({children, disabled, activeIndex, onActivate}) => {
    const mappedChildren = React.Children.map(children, (child, index) => {
        return React.cloneElement(child, {
            isActive: index === activeIndex,
            action: onActivate
        })
    })

    return (
        <div
            className='tabs'
        >
            {mappedChildren}
        </div>
    )
}

export default TabList;