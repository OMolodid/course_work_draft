import React from 'react';
import './confirm.css';

const Confirm = ({ show, title, message, textarea, textareaHandler, closeMessage, confirmMessage, disabled, children }) => {

    
        if (!show) {
            return null;
        } else {
            return (
                <>
                    <div className="backdrop" >
                        <div className="modal" >
                            <h2> {title} </h2>
                            <div> {message}</div>
                            {textarea ?
                                (<input
                                    type='textarea'
                                    onChange={textareaHandler}>
                                </input>) : (null)}

                            <div className="footer">
                                {children}
                            </div>
                        </div>
                    </div>
                </>
            )
        }

    

}

export default Confirm;