import React from 'react';
import './loader.css';

const Loader = ({type, message}) => {
    // let loader = null;
    switch (type) {
        case 'ring':
        return (
            <div>
                <div className="message-container">{message}</div>
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
        )
        // loader = `${<div class="lds-ring"><div></div><div></div><div></div><div></div></div>}`
        
          break;
        case 'heart': 
        return (
            <div>
                <div className="message-container">{message}</div>
                <div class="lds-heart"><div></div></div>
            </div>
        )   
        // loader = `${<div class="lds-heart"><div></div></div>}`
        
          break;
        case 'spinner':
        return (
            <div>
                <div className="message-container">{message}</div>
                <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
        )          
        // loader = `${<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>}`
          break;
        default:
        return (
            <div>
                <div className="message-container">{message}</div>
                <div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
        )   
        // loader = `${<div class="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>}`
      }
// return (
//     <div>
//         <div className="message-container">{message}</div>
//         {loader}
//     </div>
// )

}

export default Loader;