import React from 'react'

const Slide = ({ src, name }) => {
 
    
    return <div className="slide" ><img src={src} alt={name}></img></div>
}

export default Slide;