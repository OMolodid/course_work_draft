import React from 'react';

const RightArrow = ({action}) => {
    return (
        <div className="arrow"
        onClick={action}>
            <i className="fas fa-arrow-circle-right"></i>
        </div>
    );
}

export default RightArrow;