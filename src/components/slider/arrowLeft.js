import React from 'react';

const LeftArrow = ({action}) => {
    return (
        <div className="arrow"
        onClick={action}>
            <i class="fas fa-arrow-circle-left"></i>
        </div>
    );
}

export default LeftArrow;