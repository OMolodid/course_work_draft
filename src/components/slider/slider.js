
import React, { Component } from 'react'
import Slide from './slide'
import LeftArrow from './arrowLeft'
import RightArrow from './arrowRight'

import './slider.css'

class Slider extends Component {

    state = {

        currentIndex: 0
    }

    goToPrevSlide = () => {
        const { images } = this.props;
        const { currentIndex } = this.state;
        if (currentIndex <= 0) {
            this.setState({
                currentIndex: images.length - 1
            })
        } else {
            this.setState({
                currentIndex: this.state.currentIndex - 1
            })
        }

    }

    goToNextSlide = () => {
        const { images } = this.props;
        const { currentIndex } = this.state;
        if (currentIndex < images.length - 1) {
            this.setState({
                currentIndex: this.state.currentIndex + 1
            })

        } else {
            this.setState({
                currentIndex: 0
            })
        }

    }

    render() {
        const { images } = this.props;
        const { currentIndex } = this.state;
        const imgToShow = images[currentIndex];

        return (
            <div className="slider">
                <LeftArrow
                    action={this.goToPrevSlide}
                />

                {
                    <Slide src={imgToShow.src} alt={imgToShow.name} />
                }
                <RightArrow
                    action={this.goToNextSlide}
                />
            </div>
        );
    }
}

export default Slider;