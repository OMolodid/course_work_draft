import React from 'react';
import PropTypes from 'prop-types';
import './tabs.css'


class Tabs extends React.Component {
    state = {
        selected: this.props.selected || 0
    }

    handleChange = (index) => () => {
        this.setState({ selected: index })
    }

    render() {
        const { children } = this.props;
        return (
            <div className='wrap'>
                <ul className="inline">
                    {children.map((elem, index) => {
                        let style = index == this.state.selected ? 'selected' : '';
                        if (!elem.props.disabled) {
                            return <li
                                className={style + ' tabs-li'}
                                key={index}
                                onClick={this.handleChange(index)}
                            >
                                {elem.props.title}
                            </li>
                        }
                        return <li
                            className='disabled tabs-li'
                            key={index}
                            onClick={null}
                        >
                            {elem.props.title}
                        </li>
                    })}
                </ul>
                <div
                    className="tab"
                >
                    {children[this.state.selected]}
                </div>
            </div>
        )
    }

}

Tabs.propTypes = {


    children: PropTypes.any.isRequired,
    handleChange: PropTypes.func.isRequired,
};


export default Tabs;