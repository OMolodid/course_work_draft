import React from 'react';
import PropTypes from 'prop-types';


const Tab = (props) => (
      <div>{props.children}</div>
   )

   Tab.propTypes = {


    children: PropTypes.any.isRequired,

};

  export default Tab;