import React from 'react';

const TogglerItem = ({ value, isActive, action }) => {
    return (
        <div
            className={isActive === true
                ? "togglerItem active"
                : "togglerItem"
            }
            onClick={action(value)}
        >{value}</div>
    )

}

export default TogglerItem;