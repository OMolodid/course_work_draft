import React from 'react';
import PropTypes from 'prop-types';

 const Form = ({children, action}) =>{


        return (
  
              <form>
              {
                children
            }
  
                  <p><input type="submit" className="togglerItem" onClick={action}></input></p>
                  
              </form>
        );
    };

export default Form;
