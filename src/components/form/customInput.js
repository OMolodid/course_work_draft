import React from 'react';
import PropTypes from 'prop-types';
import './form.css'


const CustomInput = ({ name, type, placeholder, value, action }) => {

    return (
        <div className="itemContainer">
            <div>{name}</div>
            <input
            className='input'
                data-name={name}
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={action}
            />
        </div>

    )

}


CustomInput.propTypes = {


    placeholder: PropTypes.string,
    handler: PropTypes.func,
    type: PropTypes.oneOf(['text', 'password', 'number']),
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.any,
    ]),
};


export default CustomInput;