import React from 'react';
import './form.css'

const Toggler = ({children, togglerChange, name, value}) => {
    console.log(value);
         
    return (
        <div className="itemContainer">
        <div>{name}</div>
        <div className="togglerContainer "
        data-name={name}>
        
            {
                React.Children.map( children, Toggler => {
                    return React.cloneElement(
                        Toggler,
                        {
                            action: togglerChange,
                            isActive: value === Toggler.props.value
                        }
                    );
                })
            }
        </div>
        </div>
    )
}



export default Toggler;
