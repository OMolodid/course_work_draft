import React from 'react';

const Icon = ({icon}) => (


<i className={icon}></i>

)

Icon.defaultProps = {
icon: 'fab fa-angellist'
}

export default Icon 
