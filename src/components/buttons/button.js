import React from 'react';
import Icon from './icon';
import './buttons.css';


const MyButton = ({ status, type, size, icon, action, children }) => {
    let style = null;

switch (size) {
    case 'medium':
    style = {
        width: '80px',
        height: '30px',
        fontSize: '12px'
    }
      break;
    case 'small':
    
    style = {
        width: '50px',
        height: '20px',
        fontSize: '10px'
    }
    
      break;
    case 'big':
    style = {
        width: '100px',
        height: '50px',
        fontSize: '14px'
    };
      break;
    default:
    style = {
        width: '80px',
        height: '30px',
        fontSize: '12px'
    }
  }

  return(
    <button
        disabled={(status==='active' ? false : true)}
        className={(status==='active') ? type : type + ' disabled'} 
        style={style}
        onClick={action}
    >
    {icon && <Icon type={icon}/>}

        {children}
        </button>

  )
  }


MyButton.defaultProps = {
    status: 'active',
    type: 'default',
    size: 'medium',
    // icon: '#',
    action: () => {
        console.log('Default action on Button');
    },
    // children: "Just a button"
}

export default MyButton