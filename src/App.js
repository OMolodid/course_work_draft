import React, { Component } from 'react';
import {
  BrowserRouter,
  Route,
  Link,
  Switch
} from 'react-router-dom'

import './App.css';
import MyButton from './components/buttons/button';
import Tabs from './components/tabs/tabs';
import Tab from './components/tabs/tab';
import Breadcrumbs from './components/breadcrumbs/breadcrumbs';
import Posts from './components/posts';
import MyPosts from './components/myPosts';
import Loader from './components/loader/loader';
import Confirm from './components/confirm/confirm';
import Slider from './components/slider/slider';
import Card from './components/card/card';
import Cell from './components/table/cell';
import Row from './components/table/row';
import Table from './components/table/table';
import SidebarItem from './components/sidebar/sidebarItem';
import Sidebar from './components/sidebar/sidebar';
import CustomInput from './components/form/customInput';
import Toggler from './components/form/toggler';
import TogglerItem from './components/form/togglerItem';


const routes = [
  {
    link: '/',
    exact: true,
    name: 'Home'
  },
  {
    link: '/posts',
    exact: true,
    name: 'posts',
    component: Posts
  },
  {
    link: '/myposts',
    exact: true,
    name: 'My posts',
    component: MyPosts
  }
]
const test = [
  {
    link: '/',
    name: 'Home',
    icon: 'fas fa-biking'
  },
  {
    link: '/posts',
    name: 'posts',
    icon: 'fas fa-biking'
  },
  {
    link: '/myposts',
    name: 'My posts',
    icon: 'fas fa-biking'
  }
]

const images = [
  {
    name: 'aurora',
    src: "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/aurora.jpg"
  },
  {
    name: 'canyon',
    src: "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/canyon.jpg"
  },
  {
    name: 'city',
    src: "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/city.jpg"
  },
  {
    name: 'desert',
    src: "https://s3.us-east-2.amazonaws.com/dzuz14/thumbnails/desert.jpg"
  }
]


class App extends Component {
  state = {
    textareaValue: '',
    isOpen: false,
    showSlidebar: false,
    name: "",
    password: "",
    gender: "male",
    age: "",
    layout: "left",
    language: ""

  }

  sayHi = () => (console.log("Hello"));

  handleConfirmComment = (e) => {
    this.setState({ textareaValue: e.target.value })
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  hideConfirm = () => {
    this.setState({
      isOpen: false
    });
  }

  toggleSlidebar = () => {
    this.setState({
      showSlidebar: !this.state.showSlidebar
    });
    console.log(this.state.showSlidebar);

  }

  showData = (e) => {
    e.preventDefault();
    console.log("name: ", this.state.name,
      ", password: ", this.state.password,
      ", gender: ", this.state.gender,
      ", age: ", this.state.age,
      ", layout: ", this.state.layout,
      ", language: ", this.state.language);
  };

  togglerChange = (togglerName) => (value) => () => {
    this.setState({
      [togglerName]: value
    });
  }

  changeValue = inputName => e => {
    this.setState({ [inputName]: e.target.value });
  };

  render = () => {

    return (
      <div className="App">

        <BrowserRouter>
          <Breadcrumbs array={routes} />

          <Sidebar
            show={this.state.showSlidebar}
            action={this.toggleSlidebar}
            title='Sidebar Test'
          >
            <SidebarItem
              array={test} />
          </Sidebar>
          <div className='form'>
            <CustomInput
              name="name"
              type="text"
              placeholder="Enter your name"
              action={this.changeValue('name')}
            >
            </CustomInput>

            <CustomInput
              name="password"
              type="password"
              placeholder="Enter your password"
              action={this.changeValue('password')}
            >
            </CustomInput>

            <Toggler
              name="gender"
              value={this.state.gender}
              togglerChange={this.togglerChange('gender')}
            >
              <TogglerItem value="male">
              </TogglerItem>
              <TogglerItem value="female">
              </TogglerItem>
            </Toggler>

            <CustomInput
              name="age"
              type="number"
              placeholder="Enter your age"
              action={this.changeValue('age')}
            >
            </CustomInput>


            <Toggler
              name="layout"
              value={this.state.layout}
              togglerChange={this.togglerChange('layout')}
            >
              <TogglerItem value="left">
              </TogglerItem>
              <TogglerItem value="center">
              </TogglerItem>
              <TogglerItem value="right">
              </TogglerItem>
              <TogglerItem value="baseline">
              </TogglerItem>
            </Toggler>

            <CustomInput
              name="language"
              type="text"
              placeholder="Enter your favorite Language"
              action={this.changeValue('language')}
            >
            </CustomInput>
            <p>
              <MyButton
                type='secondary'
                size='big'
                action={this.showData}
              > Submit </MyButton></p>
          </div>
          <Table>
            <Row
              head={true}
            >
              <Cell
                type="money"
                color="red"
                currency="$"

              >111</Cell>
              <Cell

              />
              <Cell
                background="green"
                type="text"

              >
                456
                    </Cell>
              <Cell
                type="date"

              >123332</Cell>
            </Row>
            <Row>
              <Cell
                cells={3}

              />
              <Cell >333</Cell>
              <Cell
                type="number"

              />
              <Cell
                type="money"
                currency="£"
              >
                123
                    </Cell>
            </Row>
            <Row

            >
              <Cell>
                jjkjljkj
            </Cell>
              <Cell >333</Cell>
              <Cell
                type="number"

              />
              <Cell
                type="money"
                currency="£"
              >
                123
        </Cell>
            </Row>
          </Table>
          <Card
            size='small'
            src='https://storage.googleapis.com/chydlx/codepen/blog-cards/image-2.jpg'
            title='Hello'
            subtitle='glad to see you'
            content='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          >

            <MyButton
              size='big'
            >
              My Button
      </MyButton>
            <MyButton
              size='big'
            >
              One more Button
      </MyButton>
          </Card>
          <Card
            size='medium'
            src='https://storage.googleapis.com/chydlx/codepen/blog-cards/image-2.jpg'
            title='Hello'
            subtitle='glad to see you'
            content='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          >

            <MyButton
              size='big'
            >
              My Button
      </MyButton><MyButton

              size='big'
            >
              One more Button
      </MyButton>
          </Card>
          <Card
            size='big'
            src='https://storage.googleapis.com/chydlx/codepen/blog-cards/image-2.jpg'
            title='Hello'
            subtitle='glad to see you'
            content='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
          >

            <MyButton
              size='big'
            >
              My Button
      </MyButton><MyButton

              size='big'
            >
              One more Button
      </MyButton>
          </Card>

          <MyButton
            status='disabled'
            size='big'
          >
            My Button
      </MyButton>

          <MyButton
            type='secondary'
            size='big'
            icon
            action={this.toggleModal}
          > Open Confirm </MyButton>
          <MyButton
            type='primary'
            size='small'
            icon
          />
          <Tabs selected={0}>
            <Tab title="first">This is the first panel</Tab>
            <Tab title="second">
              <MyButton
                type='primary'
                size='small'
                icon
              />
            </Tab>
            <Tab title="third">This is the third panel</Tab>

            <Tab
              title="disabled"
              disabled={true}
            >
              This is disabled panel</Tab>
          </Tabs>
          <Loader
            message='loading...'
            type='heart'
          />
          <Loader
            message='still loading...'
            type='spinner'
          />
          <Loader
            message='loading again'

          />
          <Confirm
            show={this.state.isOpen}
            title="Do smsng important"
            message="Are you sure?"
            closeMessage="Back to smsng else"
            textarea={true}
            textareaHandler={this.handleConfirmComment}
          >
            <MyButton type="primary" size="medium" action={this.sayHi}> Send! </MyButton>
            <MyButton type="secondary" size="medium" action={this.hideConfirm}> Back! </MyButton>
          </Confirm>
          <Slider
            images={images} />
        </BrowserRouter>


      </div>

    )
  }
}

export default App;
